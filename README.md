# Highly Opinionated Guide to Fusion Middleware Administration


## VERY ROUGH DRAFT

This book is in a VERY rough draft.  I am literally throwing my unwarranted opinions on the tools as I work my way through the various pieces of it.  There **will** be many spelling, punctuation, and grammatical errors everywhere.  In addition, I don't expect the fit and finish of this to be there for months.  That said, if there's something meaningful in there, you should be able to find it hiding underneath all my over-reaching linguistic aphorisms and bromides.  But seriously, there should be something of value here, so skim with some skepticism and a critical eye.

This book will walk you through some of my lessons learned while becoming an administrator of Oracle's Fusion Middleware products.  I hope to save you a lot of headache in your journey.


## What does this book cover?


I have one simple goal--to make jumping in to the Oracle Middleware world manageable.  I don't expect you to become an expert in the system just by reading this, because **I** am not an expert.  That said, you should walk away with a lot of good tips, pointers and a general direction on what to learn next.

We will cover a myriad of topics in Oracle's Fusion Middleware Suite.  Specifically, I want to steer you towards the better parts of the platform and away from the buggier, less reliable bits.  We will discuss tools, features, and functions of the following products:
* Oracle SOA Suite
* Oracle Service Bus
* WebLogic Server
  * NodeManager
  * JVM Flags, Logging, and Heap Analysis
* Business Transaction Management
* Oracle Enterprise Manager 12c
  * Monitoring
  * Metric Extensions
  * Jobs

I am mostly familiar with the 11g versions of most of these tools, but I expect many of these tips to apply equally well to the 12c versions.

### Sections

I've divided this book into separate sections based on a tool, product, or component in that product.  Feel free to wander about, looking at each section on it's own.  *This isn't a book on using the tools.*  If you want to learn how to use those tools, I highly suggest you sign up for training from Oracle, or buy every book under the sun, or reading the copious amounts of documentation on [Oracle's own site](https://docs.oracle.com/).  Take these ramblings as a sign that someone else lost their marbles learning what worked the best in Fusion Middleware Administration.  I hate making mistakes--you shouldn't have to.

## Why did you write this?

I have an interesting backstory, so I can't say my path to this was anything but circuitous.

Prior to this role, I worked mostly on developing features for a market-based management tool.  Knowing about this particular market, how the tool lined up with, etc., required a significant amount of effort.  In another life, I am also an active member serving in the United States Armed Forces.  This is in a Reserve component, where you normally hear about the "One weekend a month, two weeks a year" mantra that is sold to new entrants as a "minor" sacrifice to make to serve your country.  Anyways, while on my most recent deployment to Afghanistan (note the **minor** sacrifice), my boss at my employer back home told me about a new opportunity.  This team was growing and needing significant technical expertise in Java, development, and Middleware administration.  As my options back in Afghanistan were limited, I decided that, "sure, why not?" woudl be a good reply.

After returning from this deployment, I was quickly thrown in a team that was preparing to train a number of employees on tools related to Oracle SOA Suite, Service Bus, WEbLogic, etc., and I was to get most/all of that training.  Prior to this, I'd only dabbled with WebLogic insofar as I was capable of restarting a Windows machine that had configured it to run as a Windows Service on boot...

This new role exposed me to many aspects of how Oracle wants you to use their products and tools.  I also found several features these tools provide are either half-baked or broken.  This by no means implies that these tools aren't *useful*, but there are certainly aspects they don't tell you about prior to using them.  When choosing between features A, B, and C, you hope that your choice will provide not just good features, but maintainability, manageability, expandability, etc..  That's not always the case.

Let me provide you those "missing notes" on what works where, and why you should use it.

## Book Recommendations

I don't want you to even start thinking of this as a *Fusion Middleware Admin for Dummies* book.  It's not.  It's a collection of gotchas, tips, shortcuts, and guidance on where to **go** so you aren't scrambling to get where you want to be.  Where **DO** you want to be?  Maybe this is a temporary thing for you, being pulled in to help another team needing help.  Maybe you are coming from a completely different field (even outside of the Java landscape) or mindset.  I don't necessarily know.  I just hope you can get far enough along in your journey that you feel comfortable working with the system, rather than against it.


## Contact

These opinions are mine, and not that of my employer.  I have not been paid by anyone to say this, nor would I want to. I've earned these opinions through my own blood, sweat, and tears (interpret that word in both forms if you want), so there is an implicit bias.  Once you've stepped through the looking glass, it's a very weird world, indeed.

If you find yourself needing to reach out to me to discuss or critique my opinions, I can be reached through email: [klauer@gmail.com](mailto:klauer@gmail.com)

I'm also lurking on Twitter [@klauern](https://twitter.com/klauern), tweeting occassionally.

Let's get through this together. Just kidding...unless you really are struggling.  Don't worry, we've all been tehre. ;)