# Business Transaction Management 12c (aka Application Performance Management)

BTM is one of those few products that I expect few teams to have seen or implemented in their organization.  The reasons for this vary, but generally speaking, if you find yourself wanting to manage a performace among a large set of interdependent services, there aren't a lot of tools to help with that.

## tl;dr Reading List

This honestly took me way more time to find than I thought reasonable.  Oracle's documentation hub looks neat, until you realize that it's very much like The Catacombs--confusing to navigate and a high chance of getting lost.

### Here's the shortcut link to the newest version of BTM:

[OEM 12c Release 5 - Associated Products](http://docs.oracle.com/cd/E24628_01/nav/assoproducts.htm)
  * [Release 12.1.0.7 Release Notes](http://docs.oracle.com/cd/E24628_01/doc.121/e59114/toc.htm)
  * [Release 12.1.0.7 Installation Guide](http://docs.oracle.com/cd/E24628_01/doc.121/e59113/toc.htm)
  * [Release 12.1.0.7 Online Help](http://docs.oracle.com/cd/E24628_01/doc.121/e59112/toc.htm)

### The longer, "How do you get there" way:

1. Go to Oracle's Docs homepage: https://docs.oracle.com/
2. Select "[Enterprise Manager](http://docs.oracle.com/en/enterprise-manager/)" (already you're probably thinking, "but I'm not interested in OEM, I'm looking for BTM"). Stay with me
3. Select [Related Products](http://docs.oracle.com/en/enterprise-manager/#tab3) tab
4. Select the link to [Business Transaction Management](http://docs.oracle.com/cd/E24628_01/nav/assoproducts.htm)
5. Select the version of product you want:
   * Release Notes
   * Installation Guide
   * Online Help


## What is it?

In the event that you stumble upon an infrastructure with BTM integrated into it, there are a couple things you need to know about it:

1. It is a tool in the suite of Application Performance Management (APM).  This means search for APM in Oracle Support should you need to file a ticket.
2. It's purpose is to monitor all service calls coming into and out of your WebLogic Server.  There are various "probes" defined that allow you to monitor different types of traffic.  These include:
  * JMS send/receive
  * JDBC open/close/execute
  * SOA Composite Callout
  * Oracle Service Bus Proxy/Business Services
  * JavaEE EJB callouts
  * Oracle Enterprise Gateway calls
3. It makes use of [Java Agents](http://docs.oracle.com/javase/8/docs/platform/jvmti/jvmti.html) to observe traffic on **all** servers you wish to monitor
4. BTM utilizes several other servers to manage your monitored traffic
5. It's heavy on database use

Let's go over a couple of these, so you know what you're in for.

### APM Suite of tools

Oracle bundles BTM under the Application Performance Management (APM) suite of tools.  The other part of the tool is called **Real User Experience Insight (RUEI)**.  RUEI will monitor usage of your User Interfaces (UI's), allowing tracking of the actual user experience in a web application.  An example of this would be monitoring 'Buy Now/Add to Cart' buttons, 'Login/Logout', etc. for responsiveness.  That tool provides *similar* timings, but they are really for User-Facing interactions.

BTM is the other part of this toolsuite.  BTM allows developers to monitor service calls between different components spread across all of your Middleware.

#### Example

Let's say you have a set of services involved in a Shopping Cart service.  Each order needs to go through several services before it is fulfilled.  As you prepare for Black Friday, your biggest concern is whether you can keep up with all the orders being sent.  You know there are many parts in the order fulfillment process.  Keeping all of these pieces humming along isn't as easy as it first sounds.  What do you do when people say orders are slow?  **Where** are they slow?  It's not enough to just keep track of "slowness" in one service or database call.  You need the full picture of the whole timing.

BTM lets you monitor service calls among disparate systems, effectively giving you a timeline for when one call from Point A, makes it's way to Point Z. It doesn't matter if it has to cross Points B, C, D, to get there, BTM will track all the interstitial calls, giving you an overall **Transaction Time.**

### Monitoring All Service Calls

As mentioned in point #2, BTM is built to *observe* many kinds of services.  If you have the observer installed on a SOA instance, you will be able to monitor calls from Composite APplications, JDBC callouts, JCA components, etc.

## Security in a Nutshell

THere are two sets of documents that explain everything there is to know about BTM security, and they can be found in separate documents:

* [BTM Installation Guide - 4 Configuring Security](http://docs.oracle.com/cd/E24628_01/doc.121/e59113/security.htm#BTMWL124)
* [BTM Online Help - 12.3 Business Transaction Management System Security](http://docs.oracle.com/cd/E24628_01/doc.121/e59112/admin.htm#BTMHP641)

The first document is very useful if you want to make sure that your data is encrypted, communicated over secure channels, and generally secure.

The second document explains how you can secure *who* can see *what* in the BTM application itself.

If you are a company that wants to use this tool among different teams, departments, or groups, you will be hard-pressed to figure out **how** to do that without installing a separate BTM system for each different group.  The documentation explains 4 high-level security roles:

* `btmAdmin` - Administrators of the system
* `btmUser` - WebLogic Operators & Monitors
* `btmObserver` - anyone else
* `btmInspector` - special group for special use

From these 4 groups, access is granted to ALL endpoints, ALL, observed messages, and transactions.  The distinction between what one group can see and another group can do is pretty minimal.  There is no way to separate access to a subset of anything, for instance.  If you want a user to be able to inspect individual messages, they will be able to inspect **all** individual messages, regardless of relevance to their department.

For this reason alone, I highly recommend taht you take great care in who you give access to this tool.  You are essentially allowing users to create and/or monitor traffic on every server you deploy the BTM observers to.  This can be pretty dangerous, and will obviously open you up to all sorts of security-related audits.

There is one highly Noted point in the documentation that should make this clear to you:

> Note: All navigation and views in the Management Console are available to all primary roles. However, some roles cannot access certain menus and menu items and the tools associated with them

That doesn't mean they can't still see the messages, but that you can prevent them from being able to see your **preferences**.