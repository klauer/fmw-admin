# Cross-Suite Tips

## Default to using WebLogic Scripting Tool (WLST)

Whether you manage one server or 50, you probably have more than one domain to make changes to.  It is not always easy to walk through the WebLogic `/console` to make your changes, 2, 3, 10, times without mistakes.  The simplest way to ensure consistent results is to apply all of these changes with a WebLogic Scripting Tool `.py` script.  I urge you to learn all you can about the Jython-based WebLogic Scripting Tool (WLST).  The way I see it, user-interfaces are for people to **discover** features of your system.  Once you know what you want to do, you should have this in a script.  I have been bitten too many times with situations like the following:

> **Bob:** Hey, did we ever put in that JMS config change on the XYZ cluster?

> **Me:** Um.  I think we put it in there.  
*spends half a day checking Dev, Test, QA and Prod domains for change*

> **Me:** Hey Bob, you know that thing you were talking about Monday?  Yeah, I was wrong, we only got to Test in that environment...

> **Bob:** can we get that up to Prod soon?

> **Me:** I'll have to schedule an outage after-hours for it...

And life continues on, mistake after mistake, always trying to scramble to make changes or keep everything consistent...

### What can you do with WLST?

Everything.  If you find something that **isn't** possible with WLST, I honestly think you should report it to Oracle Support.

A small list of things I've used WLST scripting to do:

* Deploy applications
* Configure Datasources
* Shut down a managed server
* Rebalance JMS messages across individual servers in a cluster
* Stop/Start SOA Suite Composites
* Apply one-off Security Provider configurations to an ActiveDirectory provider.

...and the list goes on and on.

### Using WLST

I'm not going to go into depth with WLST, but I think there are a few good sites that can give you 90% of the information you need to work with.

* [WLS 12c - Use WebLogic Scripting Tools](http://docs.oracle.com/middleware/1213/cross/wlsttasks.htm)
* [Record WLST scripts from `/console`](https://blogs.oracle.com/jamesbayer/entry/record_and_play_your_weblogic)
* [Store User Credentials to automate/simplify running scripts](https://docs.oracle.com/middleware/1213/wls/WLSTG/using_wlst.htm#sthref9)
* [Manage your Servers in WLST](https://docs.oracle.com/middleware/1213/wls/WLSTG/manage_servers.htm#WLSTG165)


These links alone will probably take you days to read, but they're worth it.  The sooner you can get comfortable with WLST the faster you can deliver fixes.  Life will never be the same.