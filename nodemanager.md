# NodeManager

If you're managing a cluster of servers, it's not uncommon to hear that about how great it would be to manage your servers using NodeManager.  While a commonly-held belief, it should be taken with a grain of salt.  There are some good reasons to use NodeManager:

* It will auto-restart servers based on `FAIL` scenarios
* It can migrate your WebLogic Servers between physical servers
* You find any of these reasons in their documentation to be compelling enough to pursue: [NodeManager Overview][nodemanager]

Personally, NodeManager is a tool that I find incredibly frustrating to use.  It does serve it's purpose, but does so at the cost of consistency and added complexity.  There are a couple of things to understand about NodeManager which should help you decide how best to use it.

## Process Model

NodeManager can be run as either a Script-Based tool, or as a separate JVM process.  I'll leave you to decide for yourself which version you'd rather use.  I primarily use the Java-based version, 

It is provided some configuration settings that allow it to start and stop your servers in a cluster.  It can also monitor your servers for issues that require a forcible reboot.  It is NOT a tool to manage a set of highly-consistent servers, <u>unless you're not concerned with data-loss</u>.  Assume this tool to be a small layer between the low-level Unix process and the AdminServer.

## Unfortunate Inconsistencies

The largest issue / gripe I have with NodeManager is how it handles server management compared to the AdminServer.  If you have ever used NodeManager's WebLogic Scripting Tool (WLST) interface, you know what I mean:

```python
	nmConnect('NM_USER', 'NM_PASSWORD','NM_LISTEN_ADDRESS','NM_PORT','DOMAIN_NAME','/u01/app/oracle/admin/DOMAIN_NAME/mserver/DOMAIN_NAME','<ssl or plain>')
	nmServerStatus('WLS_SOA1')
	State is 'RUNNING'
	nmKill('WLS_SOA1')
	WLS_SOA1 is STOPPED
```

Here's a couple questions about `nmKill`:

* How fast does `nmKill()` work?  
* Compared to the AdminServer's `Force Shutdown`, do you think it is faster?  
* Why would that be?  

If you guessed, 

> `nmKill` performs the equivalent of a hard kill (`kill -9`) on your underlying JVM

you're pretty much right.  This is fine if you want that, but I imagine you're looking at NodeManager and wondering, "Where's the 'Stop When Work Completes' command?"

> Where's the 'Stop when Work Completes' command?

The short answer to this is that it doesn't exist. 

> "NodeManager doesn't know anything about your server, and probably doesn't care"

NodeManager only knows the underlying processes running on your Linux/Windows machine.  This doesn't give you enough context to do the shutdown the way you want.  It can [manage crashes and auto-restart](http://docs.oracle.com/cd/E24329_01/web.1211/e21050/overview.htm#NODEM122), manage shutdowns and startups.  Surprisingly, it doesn't do it in a way that resembles what the AdminServer does.  This inconsistency is what can lead to bugs and issues.  

## NodeManager 11g versus 12c

One significant change between NodeManager in 11g WLS servers and 12c is the separation of NodeManager per domain.  You can run multiple NodeManager instances on a single domain or host.

This is great if you have a chance of running multiple domains on one host, and simplifies how it is configured.  It doesn't need to know about everything on your machine, but only needs to focus on one domain at a time.

[nodemanager]: http://docs.oracle.com/cd/E28280_01/web.1111/e13740/overview.htm#NODEM117