# Server Scale Up / Out Your Cluster
If you build a cluster in your domain, you may get comfortable enough with the `pack.sh` and `unpack.sh` commands that you don't think there's much benefit to gain by using OEMCC.  There are some benefits to this tool, and a lot to beware of as well.  We'll cover the general process of using OEMCC to expand your cluster, and perhaps cover a few issues you might encounter.

