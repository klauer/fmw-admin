# Oracle Support

I spend a lot of time playing with open-source projects, languages and tools.  I've found that in many cases, the communities that sprout up to support these tools as valuable as the tools themselves.  This is in **stark contrast** to the closed-source, closed-nature of Oracle's products (the Java Community Process excluded). Personal experiense aside, it is difficult to get an accurate estimate of what the tool is good for.  I've written this chapter specifically to help speed you along in finding answers to your questions.

This is probably one of the topics you won't find in a book on any Oracle tool.  That is, I have not seen it, and I've scoured a ton of books on Oracle's stuff.  We are to expect that all of Oracle's prodcuts will work flawlessly, as if *by magic*.  Unfortunately, this simply isn't true, but struggle with where to turn to for help.

I've seen a number of people pursue asking questions about a tool on [StackOverflow](https://www.stackoverflow.com/), Oracle's Community Forums, or general posts in a [LinkedIn](https://www.linkedin.com) group.  I can't recommend using any of these communities.  Generally speaking, people in these forums are more likely than not trying to avoid damaging the image of a tool they collect serious $$$$ to support. Why promote a bad tool if the answer is "It doesn't support that basic feature"?

## Use the Support, Luke

Oracle Support engineers will provide you an answer to your question.  It isn't a matter of making money for them.  Many of these poor souls are just trying to make a living, supporting tools that have known deficiencies and faults.  They may not know what you're struggling with, but I'm certain they've heard your sad tale before.

Here's a list of things you might actually get from a Support Request that you probably won't see in a random post on a forum:
 * An honest admission of a tool's limitations, complete with references (Doc ID's, Notes, etc.)
 * Direct responses from the development team
 * Interactions over WebEx / Video / Voice conference to show your problem to someone

I wouldn't say these are common responses, but it's a lot more frequent than anything I've ever seen elsewhere. The reasons for this are obvious:
* Support Engineers don't make money by lying to you about a tool
* You have recourse if your support request isn't being handled appropriately.
  * Should you find that the engineer is stonewalling you, shows complete lack of depth on the topic, or is just lost, you can call to have it escalated.
* Oracle takes their support seriously.  They know their tools have faults, and can't expect you to submit a pull request to fix it (they actually don't want you to, mostly for leagl reasons).  Without a strong support structure, they simply don't know where their users are struggling or what they are trying to do.
  * This has real value for them at scale.  Common complaints can be aggregated, correlated, etc., to form new products and features, solidifying their position at your company.

## How to ask

If you've seen posts on StackOverflow, you can probably spot a bad question from a good question a mile away.  If not, here's a couple things that stand out between one that is likely to get a good response, and one that is not:

### Good Questions

* Leave out uncertainty over what you were doing when the problem occurred
* Make it possible to provide different kinds of solutions
  * Perhaps the way the tool is being used is at fault
* Give EXAMPLES of ways to reproduce the error
* Are humble
  * Who do you think will get answered first?  Someone admitting their limitations in solving the problem, or someone who KNOWS that something should work but want to blame the tool, documentation, or something else for it?
  * Humility is KEY to a good dialogue.
  

### Bad Questions

* Are vague
* Don't explain thoroughly WHAT you were doing
* Expect there to be a SINGLE solution to your problem
* Give a reader little ability to **reproduce** the problem.

* Don't assume the support engineer has a deep understanding of yoru problem.
* Know what you tried, how you tried it
* Expect to provide various forms of diagnostic dumps.

## What to assume

* The Engineer is in just the same boat as you
  * Grumpy
  * Tired of bugs
  * Wants to get s$!t done
  * Hopes the next thing can be done easily


## What to Expect

* You will likely have to provide logs, diagnostic dumps using RDA (Remote Diagnostic Analyzer), OPatch logs, or versions
* You seldom get an answer right away
* If you ask at a specific time of day, you might get someone in a completely different country responding to your request (Your 4PM Support Request might be an engineer's 7AM request in Brisbane, Australia)
* Responses like "As Designed", "Won't Fix", etc., are very common based on the constraints of the problem
* Backport requests take **FOREVER**.  This is essentially the same as "Won't Fix", unless you have a timeline that is comfortable waiting **years** for a fix.
* YOu will need to upgrade to the latest patched version of the underlying product
  * It's not usually expected to upgrade from 11g to 12c or any other major release, but don't be surprised if the tool is small and obscure enough that doing so would be a net benefit for Oracle to support.
* Your design choice in using the tool may need to be reworked completely
  * I've seen on more than one occassion an engineer provide a solution that states something to the effect of "Recommended to User to use Feature X instead of Y when doing Z".  That's not necessarily a **bad** thing
  * 
  

## Tips

* Create an account on [Oracle Support](https://support.oracle.com).  The number of Doc ID's, Notes, Knowledgebase Articles, etc., on there is immense.
* Don't be too concerned with the priority level of a ticket.  I've seldom seen anything between Severity 2 - 4 be treated differently.
  * One caveat to this: Sev 4 tickets are basically treated as "answer when you have time", meaning, **never**.  Sev 2 and 3 are practically the same from my experience.