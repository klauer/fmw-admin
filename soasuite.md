# Oracle SOA Suite

Oracle SOA Suite is a complicated beast.  There's no better way to say it than that.  You have to juggle a whole host of compliexities, all interrelated and confusing to a newcomer.  If you've never taken any of the SOA Suite training offered by Oracle, as an administrator of the tool, I ***highly suggest*** you do.  It's difficult to manage a WebLogic server by itself.  SOA Suite adds a ton of features and deployments to a server that you will need to be aware of and curious to how they work:

* MetaData Store (MDS)
* BPEL Runtime Engine
* Work Managers
* XSLT/XML mapping
* Domain Value Maps (DVM)
* Sync/Async processing models
* Dehydration Store
* Databases, Databases, Databases

The single biggest part of the SOA Suite platform is the database.  This is not simply doubling-down on Oracle's bread-and-butter product.  There is an incredibly deep dependence put on the database for many features in SOA Suite.

## BPEL Auditing

## WebLogic Scriping Tool (WLST) automation



## Types of Composite Applications

* Fast and Small
* Large and Slow
* Evented
* Streaming

## Tuning your SOA environment

* JMS settings
* Database Pool
* BPEL engine
* 