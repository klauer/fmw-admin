# GC Logging

No matter how long you've been developing or maintaining servers, at some point you'll probably run across the JVM's notion of memory management.  

## GC Logging Flags

## Reference Books to read up on

Of all the books and references I've read there's one book that seems to fit the bill in being both concise and chock full of information:

* [InfoQ - The Garbage Collection Mini-Book](http://www.infoq.com/minibooks/java-garbage-collection)

I am positive there are other books out there with a lot more information, a lot more to offer regarding the JVM flags, etc., but in terms of coming to grips with how the JVM performs and manages memory, this book is pretty good.  It can bring someone reasonably out of touch with the JVM or otherwise unfamiliar with the memory model, and throw a ton of useful information on you in a short time period.

I can only point to a shortcut of tips from this book, and in all honesty, I read it and found I knew a lot about the JVM's memory prior to this.  However, there were things I was unaware of, and that book is certain to give you a lot of options to try out on your own infrastructure.