# Handy Admin Tools You Should Be Using

As you might have gathered from me, I am not a dyed-in-the-wool Oracle Fanboy.  That doesn't mean I am not a fan of the Java Virtual Machine (JVM) as a platform.  Oracle bought Sun, so I am allowed a bit of hypocrisy regarding my 

In the next few sections, I will detail some tools that you should become familiar with if you haven't already.  The wealth of tooling surrounding Java and the JVM ecosystem is **nearly unmatched** anywhere else.  I wish some of the other languages I've toyed with had half as many tools and libraries surrounding them.

## [GC Logging](tools/gclogging.md)

This section will be devoted to logging the JVM's memory activities and what you can enable with it.

## [GC Viewer](tools/gcviewer.md)

This section runs in tandem with the previous, where you can use it to analyze the GC activity.

## [Eclipse MAT](tools/eclipse-mat.md)

